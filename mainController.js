'use strict'
angular.module('myApp').controller("mainController", function ($scope) {
    $scope.UserDetails = {};
    $scope.username = 'UserName';
    $scope.userid = 99;
    $scope.GetUserName = function () {
        return "UserName";
    };
    $scope.GetUserID = function () {
        return 99;
    };
    $scope.GetUserDetails = function () {
        var username = $scope.GetUserName();
        var userid = $scope.GetUserID();
        $scope.UserDetails = {
            username: username,
            userid: userid
        };
        return $scope.UserDetails;
    };
});