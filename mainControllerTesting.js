'use strict';
describe('Filters', function(){ //describe your object type
    beforeEach(module('myApp')); //load module
    
    describe('reverse',function(){ //describe your app name
    
        var reverse;    
        beforeEach(inject(function($filter){ //initialize your filter
            reverse = $filter('reverse',{});
        }));
        
        it('Should reverse a string', function(){  //write tests
            expect(reverse('test')).toBe('tset'); //pass
        }); 
    
        it('Should reverse a string again', function(){  //write tests
            expect(reverse('don')).toBe('nod'); //pass
        }); 
        
    });
    
});